/* OffsetsAndVariables.h */

#ifndef MY_GLOBALS_H
#define MY_GLOBALS_H

/* ------- */
/* Offsets */
/* ------- */

/* Changing */
const DWORD LocalPlayer = 0xA7AFBC;
const DWORD EntityList = 0x4A1D354;
const DWORD GlowObjectBase = 0x4B30324;
const DWORD attack = 0x2E8F2B4;
const DWORD jump = 0x4AAE0C8;

/* Constant */
const DWORD m_iCrossHairID = 0x2410;
const DWORD m_iTeamNum = 0xF0;
const DWORD m_fFlags = 0x100;
const DWORD GlowIndex = 0x1DCC;
const DWORD isDormant = 0xE9;
const DWORD bSpotted = 0x935;
const DWORD flashMaxAlpha = 0x1DB0;

/* --------- */
/* Variables */
/* --------- */

/* Triggerbot */
DWORD TriggerKey;
int TriggerDelay;
bool TriggerTeamcheck;
bool TriggerUseWPM;
bool TriggerEnabled;
bool TriggerToggleCheckbox;
bool TriggerToggle;
bool TriggerBurstCheckbox;
int TriggerBurstDelay;
std::string PossibleTriggerKeyText[] = { "VK_LBUTTON", "VK_RBUTTON", "VK_LCONTROL",
"VK_RCONTROL", "VK_LEFT", "VK_RIGHT", "VK_UP", "VK_DOWN", "VK_LMENU", "VK_RMENU", "VK_LSHIFT", "VK_RSHIFT", "VK_MBUTTON",
"VK_ESCAPE", "VK_RETURN", "VK_SPACE", "VK_TAB", "VK_KEY_0", "VK_KEY_1", "VK_KEY_2", "VK_KEY_3", "VK_KEY_4", "VK_KEY_5",
"VK_KEY_6", "VK_KEY_7", "VK_KEY_8", "VK_KEY_9", "VK_KEY_A", "VK_KEY_B", "VK_KEY_C", "VK_KEY_D", "VK_KEY_E", "VK_KEY_F",
"VK_KEY_G", "VK_KEY_H", "VK_KEY_I", "VK_KEY_J", "VK_KEY_K", "VK_KEY_L", "VK_KEY_M", "VK_KEY_N", "VK_KEY_O", "VK_KEY_P",
"VK_KEY_Q", "VK_KEY_R", "VK_KEY_S", "VK_KEY_T", "VK_KEY_U", "VK_KEY_V", "VK_KEY_W", "VK_KEY_X", "VK_KEY_Y", "VK_KEY_Z",
"VK_NUMPAD0", "VK_NUMPAD1", "VK_NUMPAD2", "VK_NUMPAD3", "VK_NUMPAD4", "VK_NUMPAD5", "VK_NUMPAD6", "VK_NUMPAD7", "VK_NUMPAD8",
"VK_NUMPAD9" };
DWORD PossibleTriggerKeyDWORD[] = { 0x01, 0x02, 0xA2,
0xA3, 0x25, 0x27, 0x02, 0x28, 0xA4, 0xA5, 0xA0, 0xA1, 0x04,
0x1B, 0x0D, 0x20, 0x09, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
0x36, 0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46,
0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50,
0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A,
0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69 };

/* Glow ESP */
int GlowTeamColor;
int GlowEnemyColor;
bool GlowShowTeam;
bool GlowShowEnemys;
bool GlowEnabled;
int g_currentPlayer_GlowIndex;
int g_currentPlayer_Team;
std::string PossibleTeamColorText[] = { "Red", "Green", "Blue", "Yellow", "Orange", "Purple", "White" };
int PossibleTeamColorInt[] = { 1, 2, 3, 4, 5, 6, 7 };
std::string PossibleEnemyColorText[] = { "Red", "Green", "Blue", "Yellow", "Orange", "Purple", "White" };
int PossibleEnemyColorInt[] = { 1, 2, 3, 4, 5, 6, 7 };

/* GlowStruct for our Glow ESP */
struct GlowStruct
{
	float r;      // Red
	float g;      // Green
	float b;      // Blue
	float a;      // Alpha
	bool rwo;     // RenderWhenOccluded
	bool rwuo;    // RenderWhenUnoccluded
};

/* Bunnyhop */
DWORD BunnyKey;
bool BunnyUseWPM;
bool BunnyEnabled;
std::string PossibleBunnyKeyText[] = { "VK_LBUTTON", "VK_RBUTTON", "VK_LCONTROL",
"VK_RCONTROL", "VK_LEFT", "VK_RIGHT", "VK_UP", "VK_DOWN", "VK_LMENU", "VK_RMENU", "VK_LSHIFT", "VK_RSHIFT", "VK_MBUTTON",
"VK_ESCAPE", "VK_RETURN", "VK_SPACE", "VK_TAB", "VK_KEY_0", "VK_KEY_1", "VK_KEY_2", "VK_KEY_3", "VK_KEY_4", "VK_KEY_5",
"VK_KEY_6", "VK_KEY_7", "VK_KEY_8", "VK_KEY_9", "VK_KEY_A", "VK_KEY_B", "VK_KEY_C", "VK_KEY_D", "VK_KEY_E", "VK_KEY_F",
"VK_KEY_G", "VK_KEY_H", "VK_KEY_I", "VK_KEY_J", "VK_KEY_K", "VK_KEY_L", "VK_KEY_M", "VK_KEY_N", "VK_KEY_O", "VK_KEY_P",
"VK_KEY_Q", "VK_KEY_R", "VK_KEY_S", "VK_KEY_T", "VK_KEY_U", "VK_KEY_V", "VK_KEY_W", "VK_KEY_X", "VK_KEY_Y", "VK_KEY_Z",
"VK_NUMPAD0", "VK_NUMPAD1", "VK_NUMPAD2", "VK_NUMPAD3", "VK_NUMPAD4", "VK_NUMPAD5", "VK_NUMPAD6", "VK_NUMPAD7", "VK_NUMPAD8",
"VK_NUMPAD9" };
DWORD PossibleBunnyKeyDWORD[] = { 0x01, 0x02, 0xA2,
0xA3, 0x25, 0x27, 0x02, 0x28, 0xA4, 0xA5, 0xA0, 0xA1, 0x04,
0x1B, 0x0D, 0x20, 0x09, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
0x36, 0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46,
0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50,
0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A,
0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69 };

/* Misc */
bool NoFlashEnabled;
bool RadarEnabled;

/* Data which will be read later on in the Program */
DWORD Client;  
DWORD LocalBase;  
DWORD GlowPointer;
int LocalPlayer_Team;
int inCross;
DWORD Trigger_EntityBase;
int Trigger_EntityBase_TeamID;
byte fFlag;
float FlashMaxAlpha;

#endif

